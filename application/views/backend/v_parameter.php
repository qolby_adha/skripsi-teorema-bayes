  <?php 
    $this->load->view('backend/v_header');
  ?>
  <aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li>
          <a href="<?php echo base_url().'index.php/backend/beranda'?>">
            <i class="fa fa-home"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview active">
          <a>
            <i class="fa fa-archive"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'index.php/backend/kawasan'?>"> <i class="fa fa-list-alt"></i>Kawasan</li>
            <li class="active"><a href="<?php echo base_url().'index.php/backend/parameter'?>"><i class="fa fa-list"></i>Parameter</a></li>
            <li><a href="<?php echo base_url().'index.php/backend/klasifikasi'?>"><i class="fa fa-wrench"></i>Klasifikasi Laju Abrasi Pantai</a></li>
          </ul>
        </li>
         <li><a href="<?php echo base_url().'index.php/backend/pemetaan'?>"> <i class="fa fa-map-o"></i> <span>Peta Kawasan Abrasi</span><span class="pull-right-container"><small class="label pull-right"></small></span></a></li>
         <li>
          <a data-target="#Modalkeluar" data-toggle="modal">
            <i class="fa fa-sign-out"></i> <span>Keluar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Parameter
        <small></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
           		<a class="menu-ico">
           			<div class="col-lg-2 col-md-3 text-center services border-right">
           				<div class="service-box">
           					<h4><b>PARAMETER</b></h4>
           					<div class="ico primary">
           						  <i class="fa fa-3x fa fa-inbox wow bounceIn"></i>
           					</div>
                    <br>
           					<button data-target="#modalparameter" data-toggle="modal"  class="btn btn-primary">Lihat Data</button>
           				</div>
           			</div>
           		</a>



     
            </div>
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table">
                <thead>
                <tr>
					        <th>No</th>
					        <th>Nama Parameter</th>
                  <th>Bobot Parameter</th>
                  <th>Nama Sub Parameter</th>
                  <th>Nilai Probabilitas</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>

                <?php
                  $no=0;
                  foreach ($dataparameter->result_array() as $i) :
                     $no++;
                     $id_subparameter=$i['id_subparameter'];
                     $nm_parameter=$i['nm_parameter'];
                     $bbt_parameter=$i['bbt_parameter'];
                     $nm_subparameter=$i['nm_subparameter'];
                     $nilai_probabiltas=$i['nilai_probabilitas'];
                  ?>

                <tr>
                  <td><?php echo $no;?></td>	
                  <td><?php echo $nm_parameter;?></td>
                  <td><?php echo $bbt_parameter;?></td>
                  <td><?php echo $nm_subparameter;?></td>
                  <td><?php echo $nilai_probabiltas;?></td>

                  <td>
                    <a title="Ubah Sub Parameter" class="btn btn-primary" data-toggle="modal" data-target="#ubahsubparameter<?php echo $id_subparameter;?>"><span class="fa fa-pencil"></span></a>
                  </td>                  
                </tr>
                <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2019.</strong> All rights reserved.
  </footer>
<!---------------------Modal Parameter--------------------->
 <div class="modal fade" id="modalparameter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel">Data Parameter</h4>
          </div>
          <section class="content">
            <div class="row">                  
              <div class="box">
                <div class="box-header">
                  </div>

                  <div class="box-body">
                    <table id="example1" class="table table">
                      
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Bobot</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      
                      <tbody>
                      <?php
                        $no=0;
                        foreach ($parameter->result_array() as $i) :
                           $no++;
                           $id_parameter=$i['id_parameter'];
                           $nm_parameter=$i['nm_parameter'];
                           $bbt_parameter=$i['bbt_parameter'];
                        ?>                        
                        <tr>
                          <td><?php echo $no;?></td> 
                          <td><?php echo $nm_parameter;?></td>
                          <td><?php echo $bbt_parameter;?></td>
                          <td>
                            <a title="Ubah Parameter" class="btn btn-primary" data-toggle="modal" data-target="#ubahparameter<?php echo $id_parameter;?>"><span class="fa fa-pencil"></span></a>
                          </td>
                        </tr>
                        <?php endforeach;?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>  
            </section>
          </div>
        </div>
      </div>

<!-- Modal Parameter: Ubah Parameter -->
    <?php
      $no=0;
      foreach ($parameter->result_array() as $i) :
         $no++;
         $id_parameter=$i['id_parameter'];
         $nm_parameter=$i['nm_parameter'];
         $bbt_parameter=$i['bbt_parameter'];
      ?>  
      <div class="modal fade" id="ubahparameter<?php echo $id_parameter;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Ubah Data parameter</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/parameter/ubah_parameter'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <input type="hidden" name="xid_parameter" value="<?php echo $id_parameter;?>"/>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nama Paramter</label>
                  <div class="col-sm-7">
                    <input type="text" readonly name="xnm_parameter" class="form-control" splaceholder="Nama Parameter" value="<?php echo $nm_parameter;?>" required>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nama Parameter</label>
                  <div class="col-sm-7">
                    <input type="text" name="xbbt_parameter" class="form-control" placeholder="Nama Parameter" value="<?php echo $bbt_parameter;?>" required>
                  </div>
                </div>
              </div>              
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div> 
      <?php endforeach;?>


<!-- Modal Parameter: hapus Parameter -->
    <?php
      $no=0;
      foreach ($parameter->result_array() as $i) :
         $no++;
         $id_parameter=$i['id_parameter'];
         $nm_parameter=$i['nm_parameter'];
         $bbt_parameter=$i['bbt_parameter'];
      ?>  
      <div class="modal fade" id="hapusparameter<?php echo $id_parameter;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Hapus Data Parameter</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/parameter/hapus_parameter'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="form-group">
                  <div class="col-sm-7">
                     <input value='<?php echo $id_parameter;?>' type="hidden" name="xid_parameter"> 
                        <p>Apakah Anda yakin mau menghapus Data <b><?php echo $nm_parameter;?></b> ?</p>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <?php endforeach;?> 

<!---------------------Modal Sub Parameter--------------------->

<!-- Modal parameter : ubah sub Parameter -->
  <?php
    $no=0;
    foreach ($subparameter->result_array() as $i) :
      $no++;
      $id_subparameter=$i['id_subparameter'];
      $id_parameter=$i['id_parameter'];
      $nm_subparameter=$i['nm_subparameter'];
      $nilai_probabilitas=$i['nilai_probabilitas'];

      ?>

      <div class="modal fade" id="ubahsubparameter<?php echo $id_subparameter;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Ubah Data Sub Parameter</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/parameter/ubah_subparameter'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <input type="hidden" name="xid_subparameter" value="<?php echo $id_subparameter;?>"/>
              <div class="form-group">
                  <label class="col-sm-4 control-label">Parameter</label>
                  <div class="col-sm-7">
                      <select class="form-control select2" name="xid_parameter" required>
                          <option value="">-Pilih-</option>
                            <?php
                              $no=0;
                              foreach ($parameter->result_array() as $i) :
                                $no++;
                                    $id_parameterx=$i['id_parameter'];
                                    $nm_parameter=$i['nm_parameter'];           
                                ?>
                          <option  
                          <?php if($id_parameter==$id_parameterx) {echo "selected";} ?> value="<?php echo $id_parameterx;?>"><?php echo $nm_parameter;?> </option>
                            <?php 
                                endforeach;
                            ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nama Sub Parameter</label>
                  <div class="col-sm-7">
                    <input type="text" name="xnm_subparameter" class="form-control" id="inputUserName" placeholder="Nama Sub Parameter" value="<?php echo $nm_subparameter;?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nilai Probabilitas</label>
                  <div class="col-sm-7">
                    <input type="text" name="xprobabilitas_subparameter" class="form-control" id="inputUserName" placeholder="Nilai Probabilitas" value="<?php echo $nilai_probabilitas;?>" required>
                  </div>
                </div>
                                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div> 
    <?php endforeach;?>


  <?php 
    $this->load->view('backend/v_footer');
?>

