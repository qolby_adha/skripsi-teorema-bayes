-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Apr 2019 pada 14.20
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_teoremabayes`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_bayes`
--

CREATE TABLE `tb_bayes` (
  `id_bayes` int(11) NOT NULL,
  `hsl_bayes` double NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_desa` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `klasifikasi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_bayes`
--

INSERT INTO `tb_bayes` (`id_bayes`, `hsl_bayes`, `id_kabupaten`, `id_kecamatan`, `id_desa`, `tahun`, `klasifikasi`) VALUES
(4, 0.5328, 1, 4, 1, 0000, 'sedang'),
(5, 0.38295, 1, 4, 1, 2017, 'sedang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_desa`
--

CREATE TABLE `tb_desa` (
  `id_desa` int(11) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `nm_desa` text NOT NULL,
  `polygon_desa` text NOT NULL,
  `marker_desa` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_desa`
--

INSERT INTO `tb_desa` (`id_desa`, `id_kabupaten`, `id_kecamatan`, `nm_desa`, `polygon_desa`, `marker_desa`) VALUES
(1, 1, 4, 'Pekik Nyaring', 'a', 'a');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kabupaten`
--

CREATE TABLE `tb_kabupaten` (
  `id_kabupaten` int(11) NOT NULL,
  `nm_kabupaten` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kabupaten`
--

INSERT INTO `tb_kabupaten` (`id_kabupaten`, `nm_kabupaten`) VALUES
(1, 'Bengkulu Tengah'),
(2, 'Bengkulu Utara');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kecamatan`
--

CREATE TABLE `tb_kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `nm_kecamatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kecamatan`
--

INSERT INTO `tb_kecamatan` (`id_kecamatan`, `id_kabupaten`, `nm_kecamatan`) VALUES
(4, 1, 'Pondok Kelapa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_klasifikasi`
--

CREATE TABLE `tb_klasifikasi` (
  `id_klasifikasi` int(11) NOT NULL,
  `id_bayes` int(11) NOT NULL,
  `id_subparameter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_klasifikasi`
--

INSERT INTO `tb_klasifikasi` (`id_klasifikasi`, `id_bayes`, `id_subparameter`) VALUES
(1, 4, 21),
(2, 4, 23),
(3, 4, 31),
(4, 4, 28),
(5, 4, 25),
(6, 5, 20),
(7, 5, 19),
(8, 5, 31),
(9, 5, 29),
(10, 5, 25);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_parameter`
--

CREATE TABLE `tb_parameter` (
  `id_parameter` int(11) NOT NULL,
  `nm_parameter` varchar(32) NOT NULL,
  `bbt_parameter` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_parameter`
--

INSERT INTO `tb_parameter` (`id_parameter`, `nm_parameter`, `bbt_parameter`) VALUES
(2, 'Kecepatan Arus', 0.3),
(3, 'Tinggi Gelombang', 0.3),
(4, 'Bentuk Garis Pantai', 0.15),
(5, 'Tutupan Lahan', 0.15),
(6, 'Tipologi Pantai', 0.1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_subparameter`
--

CREATE TABLE `tb_subparameter` (
  `id_subparameter` int(11) NOT NULL,
  `id_parameter` int(11) NOT NULL,
  `nm_subparameter` varchar(32) NOT NULL,
  `nilai_probabilitas` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_subparameter`
--

INSERT INTO `tb_subparameter` (`id_subparameter`, `id_parameter`, `nm_subparameter`, `nilai_probabilitas`) VALUES
(19, 2, 'kurang dari 2 m/s', 0.333),
(20, 3, 'Kurang Dari 1 meter', 0.333),
(21, 3, '1 m sampai 2.5 m', 0.666),
(22, 3, 'Lebih dari 2.5 m', 1),
(23, 2, '2 sampai  4 m/s', 0.666),
(24, 2, 'Lebih dari 4 m/s', 1),
(25, 4, 'Berteluk', 0.333),
(26, 4, 'Lurus- Berteluk', 0.666),
(27, 4, 'Lurus', 1),
(28, 5, 'diatas 80%', 0.333),
(29, 5, '40% sampai 80%', 0.666),
(30, 5, 'dibawah 40%', 1),
(31, 6, 'Berbatu Karang', 0.333),
(32, 6, 'Berbatu Pasir', 0.666),
(33, 6, 'Berlumpur', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nm_user` varchar(32) NOT NULL,
  `pwd_user` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nm_user`, `pwd_user`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_bayes`
--
ALTER TABLE `tb_bayes`
  ADD PRIMARY KEY (`id_bayes`);

--
-- Indeks untuk tabel `tb_desa`
--
ALTER TABLE `tb_desa`
  ADD PRIMARY KEY (`id_desa`),
  ADD KEY `id_kecamatan` (`id_kecamatan`);

--
-- Indeks untuk tabel `tb_kabupaten`
--
ALTER TABLE `tb_kabupaten`
  ADD PRIMARY KEY (`id_kabupaten`);

--
-- Indeks untuk tabel `tb_kecamatan`
--
ALTER TABLE `tb_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`),
  ADD KEY `id_kabupaten` (`id_kabupaten`);

--
-- Indeks untuk tabel `tb_klasifikasi`
--
ALTER TABLE `tb_klasifikasi`
  ADD PRIMARY KEY (`id_klasifikasi`),
  ADD KEY `id_bayes` (`id_bayes`),
  ADD KEY `id_subparameter` (`id_subparameter`);

--
-- Indeks untuk tabel `tb_parameter`
--
ALTER TABLE `tb_parameter`
  ADD PRIMARY KEY (`id_parameter`);

--
-- Indeks untuk tabel `tb_subparameter`
--
ALTER TABLE `tb_subparameter`
  ADD PRIMARY KEY (`id_subparameter`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_bayes`
--
ALTER TABLE `tb_bayes`
  MODIFY `id_bayes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_desa`
--
ALTER TABLE `tb_desa`
  MODIFY `id_desa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_kabupaten`
--
ALTER TABLE `tb_kabupaten`
  MODIFY `id_kabupaten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_kecamatan`
--
ALTER TABLE `tb_kecamatan`
  MODIFY `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_klasifikasi`
--
ALTER TABLE `tb_klasifikasi`
  MODIFY `id_klasifikasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tb_parameter`
--
ALTER TABLE `tb_parameter`
  MODIFY `id_parameter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_subparameter`
--
ALTER TABLE `tb_subparameter`
  MODIFY `id_subparameter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
