  <?php 
    $this->load->view('backend/v_header');
  ?>
  <aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li>
          <a href="<?php echo base_url().'index.php/backend/beranda'?>">
            <i class="fa fa-home"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview active">
          <a>
            <i class="fa fa-archive"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'index.php/backend/kawasan'?>"> <i class="fa fa-list-alt"></i>Kawasan</li>
            <li><a href="<?php echo base_url().'index.php/backend/parameter'?>"><i class="fa fa-list"></i>Parameter</a></li>
            <li class="active"><a href="<?php echo base_url().'index.php/backend/klasifikasi'?>"><i class="fa fa-wrench"></i>Klasifikasi Laju Abrasi Pantai</a></li>
          </ul>
        </li>
         <li><a href="<?php echo base_url().'index.php/backend/pemetaan'?>"> <i class="fa fa-map-o"></i> <span>Peta Kawasan Abrasi</span><span class="pull-right-container"><small class="label pull-right"></small></span></a></li>
         <li>
          <a data-target="#Modalkeluar" data-toggle="modal">
            <i class="fa fa-sign-out"></i> <span>Keluar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ubah Data Klasifiaksi
        <small></small>
      </h1>
    </section>
		

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
                         <h4 class="modal-title" id="myModalLabel">Data Klasififkasi</h4>
            </div>

            <?php
                  foreach ($klasifikasi->result_array() as $i) :
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtgelombang= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'0','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtarus= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'1','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dttipologi= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'3','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtgaris= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'2','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtvegetasi= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'4','id_bayes'=>$i['id_bayes']))->result_array();
                      ?>

                      <?php
                      $dtgelombang[0]['id_subparameter']; 
                      ?>                  
                      <?php 
                      $dtarus[0]['id_subparameter'];
                      ?>
                      <?php
                      $dttipologi[0]['id_subparameter'];
                      ?>
                      <?php
                      $dtgaris[0]['id_subparameter'];
                      ?>
                      <?php
                      $dtvegetasi[0]['id_subparameter'];
                      ?>

                      <?php endforeach; ?>


            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/klasifikasi/hitung_ubah_klasifikasi'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
              <input type="hidden" name="xid_bayes" value="<?php echo $id_bayes;?>"/>
  
              <div class="form-group">
                  <label class="col-sm-4 control-label">Tahun</label>                  
                  <div class="col-sm-7">
                  <input type="text" name="xtahun" value="<?php echo $tahun ?> " required class="form-control">
                  </div>
                </div>              
              <div class="form-group">
                  <label class="col-sm-4 control-label">Kabupaten</label>                  
                  <div class="col-sm-7">
                        <select class="form-control" name="xid_kabupaten" id="xid_kabupaten">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($kabupaten as $prov) {
                                ?>
                                <option <?php echo $kabupaten_selected == $prov->id_kabupaten ? 'selected="selected"' : '' ?> 
                                    value="<?php echo $prov->id_kabupaten ?>"><?php echo $prov->nm_kabupaten ?></option>
                                <?php
                            }
                            ?>
                        </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Kecamatan </label>
                  <div class="col-sm-7">
                        <select class="form-control" name="xid_kecamatan" id="xid_kecamatan">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($kecamatan as $kec) {
                                ?>
                                <option <?php echo $kecamatan_selected == $kec->id_kecamatan ? 'selected="selected"' : '' ?> 
                                     class="<?php echo $kec->id_kabupaten ?>" value="<?php echo $kec->id_kecamatan ?>"><?php echo $kec->nm_kecamatan ?></option>
                                <?php
                            }
                            ?>
                        </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Desa</label>
                  <div class="col-sm-7">  
                        <select class="form-control" name="xid_desa" id="xid_desa">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($desa as $des) {
                                ?>
                                <option <?php echo $desa_selected == $des->id_desa ? 'selected="selected"' : '' ?>  
                                    class="<?php echo $des->id_kecamatan ?>" value="<?php echo $des->id_desa ?>"><?php echo $des->nm_desa ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
              </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Gelombang</label>
                  <div class="col-sm-7">  
                      <select class="form-control select2" name="xgelombang" required>
                          <option value="">-Pilih-</option>
                            <?php

                              foreach ($gelombang->result_array() as $i) :
                                    $id_subparameterx=$i['id_subparameter'];
                                    $nm_subparameter=$i['nm_subparameter'];           
                                ?>
                          <option  
                          <?php if($dtgelombang[0]['id_subparameter']==$id_subparameterx) {echo "selected";} ?> value="<?php echo $id_subparameterx;?>"><?php echo $nm_subparameter;?> </option>
                            <?php 
                                endforeach;
                            ?>
                      </select>

                    </div>
                </div>

              <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Arus</label>
                  <div class="col-sm-7">  
                      <select class="form-control select2" name="xarus" required>
                          <option value="">-Pilih-</option>
                            <?php

                              foreach ($arus->result_array() as $i) :
                                    $id_subparameterx=$i['id_subparameter'];
                                    $nm_subparameter=$i['nm_subparameter'];           
                                ?>
                          <option  
                          <?php if($dtarus[0]['id_subparameter']==$id_subparameterx) {echo "selected";} ?> value="<?php echo $id_subparameterx;?>"><?php echo $nm_subparameter;?> </option>
                            <?php 
                                endforeach;
                            ?>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Tipologi Pantai</label>
                  <div class="col-sm-7">  
                      <select class="form-control select2" name="xtipologi" required>
                          <option value="">-Pilih-</option>
                            <?php

                              foreach ($tipologi->result_array() as $i) :
                                    $id_subparameterx=$i['id_subparameter'];
                                    $nm_subparameter=$i['nm_subparameter'];           
                                ?>
                          <option  
                          <?php if($dttipologi[0]['id_subparameter']==$id_subparameterx) {echo "selected";} ?> value="<?php echo $id_subparameterx;?>"><?php echo $nm_subparameter;?> </option>
                            <?php 
                                endforeach;
                            ?>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Vegetasi</label>
                  <div class="col-sm-7">  
                      <select class="form-control select2" name="xvegetasi" required>
                          <option value="">-Pilih-</option>
                            <?php

                              foreach ($vegetasi->result_array() as $i) :
                                    $id_subparameterx=$i['id_subparameter'];
                                    $nm_subparameter=$i['nm_subparameter'];           
                                ?>
                          <option  
                          <?php if($dtvegetasi[0]['id_subparameter']==$id_subparameterx) {echo "selected";} ?> value="<?php echo $id_subparameterx;?>"><?php echo $nm_subparameter;?> </option>
                            <?php 
                                endforeach;
                            ?>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Bentuk Garis Pantai</label>
                  <div class="col-sm-7">  
                      <select class="form-control select2" name="xgaris" required>
                          <option value="">-Pilih-</option>
                            <?php

                              foreach ($garis->result_array() as $i) :
                                    $id_subparameterx=$i['id_subparameter'];
                                    $nm_subparameter=$i['nm_subparameter'];           
                                ?>
                          <option  
                          <?php if($dtgaris[0]['id_subparameter']==$id_subparameterx) {echo "selected";} ?> value="<?php echo $id_subparameterx;?>"><?php echo $nm_subparameter;?> </option>
                            <?php 
                                endforeach;
                            ?>
                      </select>
                    </div>
                </div>
                            
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2019.</strong> All rights reserved.
  </footer>
  
  <?php 
    $this->load->view('backend/v_footer');
  ?>
