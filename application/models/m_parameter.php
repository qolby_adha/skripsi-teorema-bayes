<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class m_parameter extends CI_Model {
 

	function get_all_parameter(){
		$hsl=$this->db->query("select tb_subparameter.id_subparameter, tb_subparameter.nm_subparameter, tb_subparameter.nilai_probabilitas,tb_subparameter.id_parameter, tb_parameter.id_parameter,tb_parameter.nm_parameter, tb_parameter.bbt_parameter
			from tb_subparameter
			JOIN tb_parameter on tb_subparameter.id_parameter = tb_parameter.id_parameter");
	 	return $hsl;	
	}

//--------parameter--------//
	function get_parameter(){
		$hsl=$this->db->query("Select * from tb_parameter");
		return $hsl;
	}

	function simpan_parameter($nm_parameter,$bbt_parameter){
		$hsl=$this->db->query("insert into tb_parameter(nm_parameter,bbt_parameter) values ('$nm_parameter','$bbt_parameter')");
		return $hsl;
	}
	function hapus_parameter($id_parameter){
		$hsl=$this->db->query("delete from tb_parameter where id_parameter='$id_parameter'");
		return $hsl;
	}
	function ubah_parameter($id_parameter,$nm_parameter,$bbt_parameter){

		$hsl=$this->db->query("UPDATE tb_parameter SET nm_parameter = '$nm_parameter', bbt_parameter= '$bbt_parameter' WHERE id_parameter ='$id_parameter';");
		return $hsl;
	}

//--------sub_parameter--------//
	function get_subparameter(){
		$hsl=$this->db->query("Select * from tb_subparameter");
		return $hsl;
	}

	function simpan_subparameter($id_parameter,$nm_subparameter,$nilai_probabilitas){
		$hsl=$this->db->query("insert into tb_subparameter(id_parameter,nm_subparameter,nilai_probabilitas) values ('$id_parameter','$nm_subparameter','$nilai_probabilitas')");
		return $hsl;
	}
	function hapus_subparameter($id_subparameter){
		$hsl=$this->db->query("delete from tb_subparameter where id_subparameter='$id_subparameter'");
		return $hsl;
	}
	function ubah_subparameter($id_subparameter,$id_parameterx,$nm_subparameter,$nilai_probabilitas){
		$hsl=$this->db->query("update tb_subparameter set id_parameter ='$id_parameterx',nm_subparameter='$nm_subparameter', nilai_probabilitas='$nilai_probabilitas' where id_subparameter='$id_subparameter'");
		return $hsl;
	}



}