<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
		$this->load->model('m_kawasan');
	}
	public function index()
	{
	$data['dtkabupaten']=$this->m_kawasan->get_all_kabupaten();
		$data['dtkecamatan']=$this->m_kawasan->get_kecamatan();

		$tahun=$this->input->get('2016');
		$kabupaten=$this->input->get('1');
		$this->db->order_by('tahun','DESC');
		$tahunterakhir=$this->db->get('tb_bayes')->result_array();
		
		$where=array(
			'tahun'=>$tahunterakhir[0]['tahun'],
		);
		$this->db->join('tb_desa','tb_bayes.id_desa=tb_desa.id_desa');
		$this->db->join('tb_kecamatan','tb_bayes.id_kecamatan=tb_kecamatan.id_kecamatan');
		$this->db->join('tb_kabupaten','tb_bayes.id_kabupaten=tb_kabupaten.id_kabupaten');
		$this->db->join('tb_klasifikasi','tb_bayes.id_bayes=tb_klasifikasi.id_bayes');

		$data['klasifikasi']=$this->db->get_where('tb_bayes',$where);

		$this->db->join('tb_desa','tb_bayes.id_desa=tb_desa.id_desa');

		$this->load->view('frontend/index',$data);
	}

	public function coba(){

		  $kluster1=$this->db->query('SELECT  tahun,klasifikasi,count(klasifikasi) as jumlah FROM `tb_bayes` WHERE klasifikasi="rendah" GROUP by tahun')->result_array();
		  $kluster2=$this->db->query('SELECT  tahun,klasifikasi,count(klasifikasi) as jumlah FROM `tb_bayes` WHERE klasifikasi="sedang" GROUP by tahun')->result_array();
		  $kluster3=$this->db->query('SELECT  tahun,klasifikasi,count(klasifikasi) as jumlah FROM `tb_bayes` WHERE klasifikasi="tinggi" GROUP by tahun')->result_array();   
		  $kluster[2016]['c1']=0;
		  $kluster[2017]['c1']=0;
		  $kluster[2018]['c1']=0;
		  $kluster[2016]['c2']=0;
		  $kluster[2017]['c2']=0;
		  $kluster[2018]['c2']=0;
		  $kluster[2016]['c3']=0;
		  $kluster[2017]['c3']=0;
		  $kluster[2018]['c3']=0;
		  foreach($kluster1 as $c1){
		  	if($c1['tahun']==2016){
		  		$kluster[2016]['c1']=$c1['jumlah'];
		  	}
		  	if($c1['tahun']==2017){
		  		$kluster[2017]['c1']=$c1['jumlah'];
		  	}
		  	if($c1['tahun']==2018){
		  		$kluster[2018]['c1']=$c1['jumlah'];
		  	}
		  }

		  foreach($kluster2 as $c2){
		  	if($c2['tahun']==2016){
		  		$kluster[2016]['c2']=$c2['jumlah'];
		  	}
		  	if($c2['tahun']==2017){
		  		$kluster[2017]['c2']=$c2['jumlah'];
		  	}
		  	if($c2['tahun']==2018){
		  		$kluster[2018]['c2']=$c2['jumlah'];
		  	}
		  }

		  foreach($kluster3 as $c3){
		  	if($c3['tahun']==2016){
		  		$kluster[2016]['c3']=$c3['jumlah'];
		  	}
		  	if($c3['tahun']==2017){
		  		$kluster[2017]['c3']=$c3['jumlah'];
		  	}
		  	if($c3['tahun']==2018){
		  		$kluster[2018]['c3']=$c3['jumlah'];
		  	}
		  }



	}
}
