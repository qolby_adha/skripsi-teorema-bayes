<?php
    $this->load->view('backend/v_header');
  ?>
  <aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li>
          <a href="<?php echo base_url().'index.php/backend/beranda'?>">
            <i class="fa fa-home"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-archive"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'index.php/backend/kawasan'?>"> <i class="fa fa-list-alt"></i>Kawasan</li>
            <li><a href="<?php echo base_url().'index.php/backend/parameter'?>"><i class="fa fa-list"></i>Parameter</a></li>
            <li><a href="<?php echo base_url().'index.php/backend/klasifikasi'?>"><i class="fa fa-wrench"></i>Klasifikasi Laju Abrasi Pantai</a></li>
          </ul>
        </li>
         <li class="active"><a href="<?php echo base_url().'index.php/backend/pemetaan'?>"> <i class="fa fa-map-o"></i> <span>Peta Kawasan Abrasi</span><span class="pull-right-container"><small class="label pull-right"></small></span></a></li>
         <li>
          <a data-target="#Modalkeluar" data-toggle="modal">
            <i class="fa fa-sign-out"></i> <span>Keluar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.s -->
      </aside>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pemetaan Tingkat Rawan Bencana Laju Abrasi Pantai Bengkulu Tengah dan Bengkulu Utara
        <small></small>
      </h1>

    </section>

    <!-- Main content -->

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
            <div class="box-header">
              <a class="menu-ico">
                <div class="col-lg-2 col-md-3 text-center services border-right">
                  <div class="service-box">
                    <h4><b>Tahun</b></h4>
                    <br>
                      <form class="form-inline" method="get" action="<?php echo base_url('index.php/backend/pemetaan/index/') ?>">
                        <select  name="tahun" id="tahun">
                        <option>
                          pilih
                        </option>
                          <?php
                            $this->db->group_by('tahun');
                            $query = $this->db->get('tb_bayes')->result_array();
                            foreach($query as $key){
                           ?>
                            <option><?php echo $key['tahun'] ?></option>
                         <?php } ?>
                        </select>
                  </div>
                </div>
              </a>
              <a  class="menu-ico">

                <div class="col-lg-4 col-md-3 text-center services border-right">
                  <div  class="service-box">
                    <h4><b>Kabupaten</b></h4>
                    <br>

                        <select  name="kabupaten" id="kabupaten">

                        </select>
                        <input class="btn btn-primary" type="submit" value="Pilih">
                      </form>
                  </div>
                </div>
              </a>
            </div>
            <div class="container-fluid">
              <div class="box-body">
                <div id="map" style="width:100%;height:800px;"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

    <!-- /.content -->

    <?php
    $this->load->view('backend/v_footer');
  ?>

<?php
  $data_kawasan=array();
  foreach($klasifikasi->result() as $p){
    $data_kawasan[$p->id_bayes][0]=$p->id_desa;
    $data_kawasan[$p->id_bayes][1]=$p->nm_desa;
    $data_kawasan[$p->id_bayes][2]=$p->polygon_desa;
    $data_kawasan[$p->id_bayes][4]=$p->klasifikasi;
    $data_kawasan[$p->id_bayes][5]=$p->tahun;
    $data_kawasan[$p->id_bayes][6]=$p->id_kecamatan;
    $data_kawasan[$p->id_bayes][7]=$p->nm_kecamatan;
    $data_kawasan[$p->id_bayes][8]=$p->id_kabupaten;
    $data_kawasan[$p->id_bayes][9]=$p->nm_kabupaten;
    ?>

<div class="modal modal-default fade" id="modal_<?php echo $p->id_desa?>">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><b>Informasi Klasifikasi Tingkat Rawan Bencana Laju Abrasi Pada Tahun <?php echo $p->tahun?> : Desa <?php echo $p->nm_desa?>, Kecamatan <?php echo $p->nm_kecamatan?>, Kabupaten <?php echo $p->nm_kabupaten?></b></h4>
        </div>
        <?php
          $no=0;
          foreach ($klasifikasi->result_array() as $i) :

              $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
              $dtgelombang= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'0','id_bayes'=>$i['id_bayes']))->result_array();

              $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
              $dtarus= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'1','id_bayes'=>$i['id_bayes']))->result_array();

              $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
              $dttipologi= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'3','id_bayes'=>$i['id_bayes']))->result_array();

              $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
              $dtgaris= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'2','id_bayes'=>$i['id_bayes']))->result_array();
              $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
              $dtvegetasi= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'4','id_bayes'=>$i['id_bayes']))->result_array();

            ?>
          <?php endforeach;?>
          <div class="modal-body container-fluid">
                <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                  <h3 class="box-title">Tinggi Gelombang</h3>
              </div>
              <div class="box-body">
                Sub Indikator : <br><b><?php echo $dtgelombang[0]['nm_subparameter']; ?></b>
              </div>

              <div class="box-header with-border">
                  <h3 class="box-title">Kecepatan Arus</h3>
              </div>
              <div class="box-body">
                Sub Indikator : <br><b><?php echo $dtarus[0]['nm_subparameter']; ?></b>
              </div>

              <div class="box-header with-border">
                  <h3 class="box-title">Tipologi Pantai</h3>
              </div>
              <div class="box-body">
                Sub Indikator : <br><b> <?php echo $dttipologi[0]['nm_subparameter']; ?></b>
              </div>

              <div class="box-header with-border">
                  <h3 class="box-title">Bentuk Garis Pantai</h3>
              </div>
              <div class="box-body">
                Sub Indikator : <br><b> <?php echo $dtgaris[0]['nm_subparameter'];?> </b>
              </div>

              <div class="box-header with-border">
                  <h3 class="box-title">Tutupan Vegetasi</h3>
              </div>
              <div class="box-body">
                Sub Indikator : <br><b><?php echo $dtvegetasi[0]['nm_subparameter']; ?></b>
              </div>

              <div class="box-header with-border">
                  <h3 class="box-title">Klasifikasi Tingkat Rawan Bencana</h3>
              </div>
              <div class="box-body">
                <b><?php echo $p->klasifikasi ?></b>
              </div>

            </div>
        </div>
              </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Keluar</button>
            </div>
        </div>
    </div>
</div>



<?php } ?>
<script type="text/javascript">
  function initMap(){
    var map;
    var polygon=[];
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:-3.635299, lng: 102.321318},
          zoom: 8
        });

        var data = <?php echo json_encode($data_kawasan)?>;
        var infowindow_kel=[];

        for(var x in data){
          var j = [];
          var kl = data[x][4];
          var ll = data[x][2].split("\n");


          for(var i=1;i<=ll.length;i++){
          var longlat = ll[i-1].split(",");
          j.push(new google.maps.LatLng(parseFloat(longlat[0]), parseFloat(longlat[1])));
      }


      var info = "<h3>"+data[x][1]+"</h3>"+"<button class='btn btn-primary btn-block' data-toggle='modal' data-target='#modal_"+data[x][0]+"'>Lihat Indikator</button>";


      if(kl=='rendah'){
        color="green"
      }else if(kl=='sedang'){
        color="yellow"
      }else if(kl=='tinggi'){
        color="red"
      }

      var polygon_x = new google.maps.Polygon({
        paths: j,
          strokeColor: color,
          strokeOpacity: 0.8,
          strokeWeight: 3,
          fillColor: color,
          fillOpacity: 0.35
        });

      polygon.push(polygon_x);
      infowindow = new google.maps.InfoWindow({content: info,maxWidth:1000});
        infowindow_kel.push(infowindow);
        }

        for(var m=0; m<=polygon.length;m++){
        polygon[m].setMap(map);
        google.maps.event.addListener(polygon[m], 'click', (function(m) {
          infowindow_kel[m].close();
          return function(event){
            infowindow_kel[m].setPosition(event.latLng)
            infowindow_kel[m].open(map);
        }
      })(m));
    }
    }

  </script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkVqDh3ZeTTfLOxCm557neqs51wnzg-SM&callback=initMap">
</script>