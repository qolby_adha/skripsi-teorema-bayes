<?php
class parameter extends CI_Controller {
	function __construct() 
	{
		
		parent::__construct();
//            jika belum login redirect ke login
		
		if ($this->session->userdata('logged')<>1) {
			redirect(site_url('login'));
		}
		$this->load->model('m_parameter');
	}

	public function index()
	{
		$data['dataparameter']=$this->m_parameter->get_all_parameter();	
		$data['subparameter']=$this->m_parameter->get_subparameter();	
		$data['parameter']=$this->m_parameter->get_parameter();
		
		$this->load->view('backend/v_parameter',$data);
	}
//--------parameter--------//
	function simpan_parameter(){
		$nm_parameter=strip_tags($this->input->post('xnm_parameter'));
		$bbt_parameter=strip_tags($this->input->post('xbbt_parameter'));
		$this->m_parameter->simpan_parameter($nm_parameter,$bbt_parameter);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/parameter');
	}

	function ubah_parameter(){
		$id_parameter=strip_tags($this->input->post('xid_parameter'));
		$nm_parameter=strip_tags($this->input->post('xnm_parameter'));
		$bbt_parameter=strip_tags($this->input->post('xbbt_parameter'));
		$this->m_parameter->ubah_parameter($id_parameter,$nm_parameter,$bbt_parameter);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/parameter');
	}

	function hapus_parameter(){
		$id_parameter=strip_tags($this->input->post('xid_parameter'));
		$this->m_parameter->hapus_parameter($id_parameter);
		echo $this->session->set_flashdata('msg','hapusdata');
		redirect('backend/parameter');
	}	

//--------sub _parameter--------//
	function simpan_subparameter(){
		$id_parameter=strip_tags($this->input->post('xid_parameter'));
		$nm_subparameter=strip_tags($this->input->post('xnm_subparameter'));
		$nilai_probabilitas=strip_tags($this->input->post('xprobabilitas_subparameter'));
		$this->m_parameter->simpan_subparameter($id_parameter,$nm_subparameter,$nilai_probabilitas);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/parameter');
	}
	
	function ubah_subparameter(){
		$id_subparameter=strip_tags($this->input->post('xid_subparameter'));
		$id_parameterx=strip_tags($this->input->post('xid_parameter'));
		$nm_subparameter=strip_tags($this->input->post('xnm_subparameter'));
		$nilai_probabilitas=strip_tags($this->input->post('xprobabilitas_subparameter'));
		$this->m_parameter->ubah_subparameter($id_subparameter,$id_parameterx,$nm_subparameter,$nilai_probabilitas);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/parameter');
	}

	function hapus_subparameter(){
		$id_subparameter=strip_tags($this->input->post('xid_subparameter'));
		$this->m_parameter->hapus_subparameter($id_subparameter);
		echo $this->session->set_flashdata('msg','hapusdata');
		redirect('backend/parameter');
	}
}
