  <?php 
    $this->load->view('backend/v_header');
  ?>
  <aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li>
          <a href="<?php echo base_url().'index.php/backend/beranda'?>">
            <i class="fa fa-home"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        
        <li class="treeview active">
          <a>
            <i class="fa fa-archive"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'index.php/backend/kawasan'?>"> <i class="fa fa-list-alt"></i>Kawasan</li>
            <li><a href="<?php echo base_url().'index.php/backend/parameter'?>"><i class="fa fa-list"></i>Parameter</a></li>
            <li class="active"><a href="<?php echo base_url().'index.php/backend/klasifikasi'?>"><i class="fa fa-wrench"></i>Klasifikasi Laju Abrasi Pantai</a></li>
          </ul>

        </li>

         <li><a href="<?php echo base_url().'index.php/backend/pemetaan'?>"> <i class="fa fa-map-o"></i> <span>Peta Kawasan Abrasi</span><span class="pull-right-container"><small class="label pull-right"></small></span></a></li>
         
         <li>
          <a data-target="#Modalkeluar" data-toggle="modal">
            <i class="fa fa-sign-out"></i> <span>Keluar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Klasifikasi Kawasan Bencana Laju Abrasi Pantai
        <small></small>
      </h1>
    </section>
		

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
           		<a class="menu-ico">
           			<div class="col-lg-2 col-md-3 text-center services border-right">
           				<div class="service-box">
           					<h4><b>Tambah Klasifikasi Tingkat Rawan Bancana Abrasi</b></h4>
           					<div class="ico primary">
           						  <i class="fa fa-3x fa fa-university wow bounceIn"></i>
           					</div>
                    <br>
           					<button data-target="#hitungklasifikasi" data-toggle="modal"  class="btn btn-primary">Tambah Data</button>
           				</div>
           			</div>
           		</a>

              <a class="menu-ico">
                <div class="col-lg-2 col-md-3 text-center services border-right">
                  <div class="service-box">
                    <h4><b>Export data ke Excel</b></h4>
                    <div class="ico primary">
                        <i class="fa fa-3x fa-file-excel-o wow bounceIn"></i>
                    </div>
                    <br>
                    <button data-toggle="modal" data-target="#exceltahun" class="btn btn-primary">Export</button>
                  </div>
                </div>
              </a>     
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table">
                <thead>
                <tr>
              			<th>No</th>
                    <th>Tahun</th>
              			<th>Kabupaten</th>
              			<th>Kecamatan</th>
              			<th>Desa</th>
              			<th>Tinggi Gelombang</th>
              			<th>Arus</th>
              			<th>Tipologi Pantai</th>
              			<th>Bentuk Garis Pantai</th>
              			<th>Tutupan Lahan</th>
                    <th>Klasifikasi</th>
                    <th>aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $no=0;
                  foreach ($klasifikasi->result_array() as $i) :
                     $no++;
                     $id_bayes=$i['id_bayes'];
                     $tahun=$i['tahun'];
                     $id_desa=$i['id_desa'];
                     $nm_kabupaten=$i['nm_kabupaten'];
                     $nm_kecamatan=$i['nm_kecamatan'];
                     $nm_desa=$i['nm_desa'];
                     $klasifikasi=$i['klasifikasi'];

                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtgelombang= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'0','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtarus= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'1','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dttipologi= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'3','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtgaris= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'2','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtvegetasi= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'4','id_bayes'=>$i['id_bayes']))->result_array();
                      ?>
                
                  <tr>
                  <td><?php echo $no;?></td>
                  <td><?php echo $tahun;?></td>  
                  <td><?php echo $nm_kabupaten;?></td>
                  <td><?php echo $nm_kecamatan;?></td>
                  <td><?php echo $nm_desa;?></td>

                  <td><?php
                    echo $dtgelombang[0]['nm_subparameter']; 
                   ?></td>
                  
                  <td>
                   <?php 
                    echo $dtarus[0]['nm_subparameter'];
                  ?> 
                  </td>
                  
                  <td>
                    <?php 

                    echo $dttipologi[0]['nm_subparameter'];
                  ?>
                  </td>      
                  
                  <td>
                    <?php 
                    echo $dtgaris[0]['nm_subparameter'];
                  ?>
                  </td> 
                  
                  <td>
                    <?php 

                    echo $dtvegetasi[0]['nm_subparameter'];
                  ?>
                  </td>
                  <td>
                    <?php echo $klasifikasi; ?>
                  </td>
                  <td>
                    <a title="Ubah data" class="btn btn-primary" href="<?php echo base_url().'index.php/backend/klasifikasi/ubah_klasifikasi/'.$id_bayes.'/'.$id_desa.'/'.$dtgelombang[0]['id_subparameter'].'/'.$dtarus[0]['id_subparameter'].'/'.$dttipologi[0]['id_subparameter'].'/'.$dtgaris[0]['id_subparameter'].'/'.$dtvegetasi[0]['id_subparameter'].'/'.$tahun;?>"> <span class="fa fa-pencil"></span></a>
                    <a title="Hapus Data" class="btn btn-danger
                    " data-toggle="modal" data-target="#hapusklasifikasi<?php echo $id_bayes;?>"><span class="fa fa-trash"></span></a>
                  </td>
                </tr>
              <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2019.</strong> All rights reserved.
  </footer>
<!---------------------Modal Desa--------------------->       

<!-- Modal Kabupaten: tambah Klasifikasi -->
      <div class="modal fade" id="hitungklasifikasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class=" btn btn-danger fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Data Klasifikasi</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/klasifikasi/hitungklasifikasi'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">

            <div class="form-group">
              <label for="inputUserName" class="col-sm-4 control-label">Tahun Terjadi</label>
              <div class="col-sm-7">
                <input type="text" name="xtahun" class="form-control" placeholder="Tahun Terjadi" required>
              </div>
            </div>
              
              <div class="form-group">
                  <label class="col-sm-4 control-label">Kabupaten</label>
                  <div class="col-sm-7">
                    <select name="xid_kabupaten" id="xid_kabupaten" class="form-control select2" required>
                        <option value="">Pilih Kabupaten</option>
                            <?php
                            foreach ($kabupaten->result() as $kab) {
                                ?>
                                <option <?php echo $kabupaten_selected == $kab->id_kabupaten ? 'selected="selected"' : '' ?> 
                                    value="<?php echo $kab->id_kabupaten ?>"><?php echo $kab->nm_kabupaten ?></option>
                                <?php
                            }
                            ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Kecamatan </label>
                  <div class="col-sm-7">
                  <select class="form-control" name="xid_kecamatan" id="xid_kecamatan">
                    <option value="">Pilih Kecamatan</option>
                      <?php
                      foreach ($kecamatan2 as $kec) {
                          ?>
                          <option <?php echo $kecamatan_selected == $kec->id_kabupaten ? 'selected="selected"' : '' ?> 
                              class="<?php echo $kec->id_kabupaten ?>" value="<?php echo $kec->id_kecamatan ?>"><?php echo $kec->nm_kecamatan ?></option>
                          <?php
                      }
                      ?>
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Desa </label>
                  <div class="col-sm-7">
                    <select name="xid_desa" id="xid_desa" class="form-control select2" required>
                      <option value="">Pilih Desa</option>
                      <?php
                      foreach ($desax as $des) {
                          ?>
                          <option <?php echo $desa_selected == $des->id_kecamatan ? 'selected="selected"' : '' ?> 
                              class="<?php echo $des->id_kecamatan ?>" value="<?php echo $des->id_desa ?>"><?php echo $des->nm_desa ?></option>
                          <?php }?>
                    </select>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Tinggi Gelombang</label>
                  <div class="col-sm-7">
                    <select name="xgelombang" class="form-control select2" required>
                      <option value="">-Pilih-</option>
                        <?php
                          $no=0;
                          foreach ($gelombang->result_array() as $i) :
                            $no++;
                                $id_subparameter=$i['id_subparameter'];
                                $nm_subparameter=$i['nm_subparameter'];
                                $nilai_probabilitas=$i['nilai_probabilitas'];           
                            ?>
                      <option value="<?php echo $id_subparameter;?>"><?php echo $nm_subparameter;?></option>
                        <?php endforeach;?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Kecepatan Arus</label>
                  <div class="col-sm-7">
                  <select name="xarus" class="form-control select2" required>
                      <option value="">-Pilih-</option>
                        <?php
                          $no=0;
                          foreach ($arus->result_array() as $i) :
                            $no++;
                                $id_subparameter=$i['id_subparameter'];
                                $nm_subparameter=$i['nm_subparameter'];
                                $nilai_probabilitas=$i['nilai_probabilitas'];           
                            ?>
                      <option value="<?php echo $id_subparameter;?>"><?php echo $nm_subparameter;?></option>
                        <?php endforeach;?>
                    </select>  
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Tipologi Pantai</label>
                  <div class="col-sm-7">
                  <select name="xtipologi" class="form-control select2" required>
                      <option value="">-Pilih-</option>  
                        <?php
                          $no=0;
                          foreach ($tipologi->result_array() as $i) :
                            $no++;
                                $id_subparameter=$i['id_subparameter'];
                                $nm_subparameter=$i['nm_subparameter'];
                                $nilai_probabilitas=$i['nilai_probabilitas'];           
                            ?>
                      <option value="<?php echo $id_subparameter;?>"><?php echo $nm_subparameter;?></option>
                        <?php endforeach;?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Bentuk Garis Pantai</label>
                  <div class="col-sm-7">
                    <select name="xgaris" class="form-control select2" required>
                      <option value="">-Pilih-</option>
                        <?php
                          $no=0;
                          foreach ($garis->result_array() as $i) :
                            $no++;
                                $id_subparameter=$i['id_subparameter'];
                                $nm_subparameter=$i['nm_subparameter'];
                                $nilai_probabilitas=$i['nilai_probabilitas'];           
                            ?>
                      <option value="<?php echo $id_subparameter;?>"><?php echo $nm_subparameter;?></option>
                        <?php endforeach;?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Tutupan Vegetasi</label>
                  <div class="col-sm-7">
                    <select name="xvegetasi" class="form-control select2" required>
                      <option value="">-Pilih-</option>
                        <?php
                          $no=0;
                          foreach ($vegetasi->result_array() as $i) :
                            $no++;
                                $id_subparameter=$i['id_subparameter'];
                                $nm_subparameter=$i['nm_subparameter'];
                                $nilai_probabilitas=$i['nilai_probabilitas'];           
                            ?>
                      <option value="<?php echo $id_subparameter;?>"><?php echo $nm_subparameter;?></option>
                        <?php endforeach;?>
                    </select>
                  </div>
                </div>

              </div>

              <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>



  <?php 
    $this->load->view('backend/v_klasifikasi2');
  ?>
  
  <?php 
    $this->load->view('backend/v_footer');
  ?>