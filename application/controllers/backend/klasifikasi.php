<?php
class klasifikasi extends CI_Controller {
	function __construct() 
	{
		
		parent::__construct();
//            jika belum login redirect ke login
		
		if ($this->session->userdata('logged')<>1) {
			redirect(site_url('login'));
		}
		$this->load->model('m_klasifikasi');
		$this->load->model('m_kawasan');
		$this->load->model('m_parameter');

	}

	public function index()
	{
		
		$data['kabupaten']=$this->m_kawasan->get_all_kabupaten();
		$data['kecamatan']=$this->m_kawasan->get_kecamatan();
		$data['desa']=$this->m_kawasan->get_all_desa();
		$data['desa2']=$this->m_kawasan->get_all_desa()->result_array();
		$data['desax']=$this->m_kawasan->get_des();
		$data['kecamatan2']=$this->m_kawasan->get_kec();
		$data['kabupaten_selected']= '';
        $data['kecamatan_selected' ]= '';
        $data['desa_selected' ]= '';
        $data['subparameter' ]= '';

		$data['gelombang']=$this->m_klasifikasi->get_dtgelombang();
		$data['arus']=$this->m_klasifikasi->get_dtarus();
		$data['tipologi']=$this->m_klasifikasi->get_dttipologi();
		$data['garis']=$this->m_klasifikasi->get_dtgaris();
		$data['vegetasi']=$this->m_klasifikasi->get_dtvegetasi();

		$this->db->join('tb_desa','tb_desa.id_desa=tb_bayes.id_desa');
		$this->db->join('tb_kecamatan','tb_kecamatan.id_kecamatan=tb_bayes.id_kecamatan');
		$this->db->join('tb_kabupaten','tb_kabupaten.id_kabupaten=tb_bayes.id_kabupaten');
	
		$data['klasifikasi']=$this->db->get('tb_bayes');

		$this->db->join('tb_desa','tb_desa.id_desa=tb_bayes.id_desa');
		$this->db->join('tb_kecamatan','tb_kecamatan.id_kecamatan=tb_bayes.id_kecamatan');
		$this->db->join('tb_kabupaten','tb_kabupaten.id_kabupaten=tb_bayes.id_kabupaten');
		$this->db->join('tb_klasifikasi','tb_klasifikasi.id_bayes=tb_bayes.id_bayes');		
		$data['dtklasifikasi']=$this->db->get('tb_bayes');
		$this->load->view('backend/v_klasifikasi',$data);

	}
	public function index2(){
		$data['kabupaten']=$this->m_kawasan->get_all_kabupaten();
		$data['kecamatan']=$this->m_kawasan->get_kecamatan();
		$data['desa']=$this->m_kawasan->get_all_desa();
		$data['desa2']=$this->m_kawasan->get_all_desa()->result_array();
		$data['desax']=$this->m_kawasan->get_des();
		$data['kecamatan2']=$this->m_kawasan->get_kec();
		$data['kabupaten_selected']= '';
        $data['kecamatan_selected' ]= '';
        $data['desa_selected' ]= '';
        $data['subparameter' ]= '';

		$data['gelombang']=$this->m_klasifikasi->get_dtgelombang();
		$data['arus']=$this->m_klasifikasi->get_dtarus();
		$data['tipologi']=$this->m_klasifikasi->get_dttipologi();
		$data['garis']=$this->m_klasifikasi->get_dtgaris();
		$data['vegetasi']=$this->m_klasifikasi->get_dtvegetasi();

		$this->db->join('tb_desa','tb_desa.id_desa=tb_bayes.id_desa');
		$this->db->join('tb_kecamatan','tb_kecamatan.id_kecamatan=tb_bayes.id_kecamatan');
		$this->db->join('tb_kabupaten','tb_kabupaten.id_kabupaten=tb_bayes.id_kabupaten');
		$this->db->join('tb_klasifikasi','tb_klasifikasi.id_bayes=tb_bayes.id_bayes');		
		$data['dtklasifikasi']=$this->db->get('tb_bayes');
		$this->load->view('backend/v_klasifikasi2',$data);
	}


		function hitungklasifikasi(){

		echo $id_kabupaten=strip_tags($this->input->post('xid_kabupaten'));
		$id_kecamatan=strip_tags($this->input->post('xid_kecamatan'));
		$id_desa=strip_tags($this->input->post('xid_desa'));
		$tahun=strip_tags($this->input->post('xtahun'));		
		
		echo '<br>';
		echo $gelombang=strip_tags($this->input->post('xgelombang'));
		echo '<br>';
		echo $arus=strip_tags($this->input->post('xarus'));
		echo '<br>';
		echo $tipologi=strip_tags($this->input->post('xtipologi'));
		echo '<br>';
		echo $garis=strip_tags($this->input->post('xgaris'));
		echo '<br>';
		echo $vegetasi=strip_tags($this->input->post('xvegetasi'));

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$gelombang))->result_array();
		$bayes1=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$arus))->result_array();		
		$bayes2=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$tipologi))->result_array();
		$bayes3=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$garis))->result_array();
		$bayes4=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$vegetasi))->result_array();
		$bayes5=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;
		echo "<br>";
		echo $tot=$bayes1+$bayes2+$bayes3+$bayes4+$bayes5;
		
		if($tot<0.334){
			$kls="rendah";
		}elseif($tot<0.666){
			$kls="sedang";
		}else{
			$kls="tinggi";
		}

		echo $kls;

		$data=array(
			'hsl_bayes'=>$tot,
			'id_kabupaten'=>$id_kabupaten,
			'id_kecamatan'=>$id_kecamatan,
			'id_desa'=>$id_desa,
			'tahun'=>$tahun,
			'klasifikasi'=>$kls
		);

		$this->db->insert('tb_bayes',$data);

		echo $id=$this->db->insert_id();

		$data2['id_bayes']=$id;
		$data2['id_subparameter']=$gelombang;
		$this->db->insert('tb_klasifikasi',$data2);

		$data2['id_subparameter']=$arus;
		$this->db->insert('tb_klasifikasi',$data2);

		$data2['id_subparameter']=$tipologi;
		$this->db->insert('tb_klasifikasi',$data2);

		$data2['id_subparameter']=$vegetasi;
		$this->db->insert('tb_klasifikasi',$data2);

		$data2['id_subparameter']=$garis;
		$this->db->insert('tb_klasifikasi',$data2);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/klasifikasi');
	}

		function hapus_klasifikasi(){
		$id_bayes=strip_tags($this->input->post('xid_bayes'));
		$this->m_klasifikasi->hapus_klasifikasi($id_bayes);
		echo $this->session->set_flashdata('msg','hapusdata');
		redirect('backend/klasifikasi');
	}

function ubah_klasifikasi($id_bayes,$id_desa){

		$selected = $this->m_kawasan->get_desa_by_id($id_desa);
		 $row = $this->m_klasifikasi->getById($id_bayes)->row();
        $data = array(
        	'id_bayes'=>$row->id_bayes,
        	'tahun'=> $row->tahun,
            'kabupaten' => $this->m_kawasan->get_kab(),
            'kecamatan' => $this->m_kawasan->get_kec(),
            'desa' => $this->m_kawasan->get_des(),
			'kabupaten_selected' => $selected->id_kabupaten,
        	'kecamatan_selected' => $selected->id_kecamatan,
        	'desa_selected' => $selected->id_desa,
            
            'gelombang' => $this->m_klasifikasi->get_dtgelombang(),
            'arus' => $this->m_klasifikasi->get_dtarus(),
            'tipologi' => $this->m_klasifikasi->get_dttipologi(),
            'vegetasi' => $this->m_klasifikasi->get_dtvegetasi(),
            'garis' => $this->m_klasifikasi->get_dtgaris(),
            'klasifikasi' => $this->m_klasifikasi->get_detailklasifikasi(),

        );

        $this->load->view('backend/v_ubah_klasifikasi',$data);	
}

	function hitung_ubah_klasifikasi(){

		$id_bayes=$this->input->post('xid_bayes');
		
		echo $id_kabupaten=strip_tags($this->input->post('xid_kabupaten'));
		
		$id_kecamatan=strip_tags($this->input->post('xid_kecamatan'));
		
		$id_desa=strip_tags($this->input->post('xid_desa'));
		
		$tahun=strip_tags($this->input->post('xtahun'));		
		
		echo '<br>';
		echo $gelombang=strip_tags($this->input->post('xgelombang'));
		echo '<br>';
		echo $arus=strip_tags($this->input->post('xarus'));
		echo '<br>';
		echo $tipologi=strip_tags($this->input->post('xtipologi'));
		echo '<br>';
		echo $garis=strip_tags($this->input->post('xgaris'));
		echo '<br>';
		echo $vegetasi=strip_tags($this->input->post('xvegetasi'));

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$gelombang))->result_array();
		$bayes1=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$arus))->result_array();		
		$bayes2=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$tipologi))->result_array();
		$bayes3=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$garis))->result_array();
		$bayes4=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;

		$this->db->join('tb_parameter','tb_subparameter.id_parameter=tb_parameter.id_parameter');
		$parameter=$this->db->get_where('tb_subparameter',array('id_subparameter'=>$vegetasi))->result_array();
		$bayes5=$parameter[0]['nilai_probabilitas']*$parameter[0]['bbt_parameter']/1;
		echo "<br>";
		echo $tot=$bayes1+$bayes2+$bayes3+$bayes4+$bayes5;
		
		if($tot<0.334){
			$kls="rendah";
		}elseif($tot<0.667){
			$kls="sedang";
		}else{
			$kls="tinggi";
		}

		echo $kls;

		$data=array(
			'hsl_bayes'=>$tot,
			'id_kabupaten'=>$id_kabupaten,
			'id_kecamatan'=>$id_kecamatan,
			'id_desa'=>$id_desa,
			'tahun'=>$tahun,
			'klasifikasi'=>$kls
		);

		$this->db->update('tb_bayes',$data,array('id_bayes'=>$id_bayes));

		$this->db->delete('tb_klasifikasi',array('id_bayes'=>$id_bayes));

		$data2['id_bayes']=$id_bayes;
		$data2['id_subparameter']=$gelombang;
		$this->db->insert('tb_klasifikasi',$data2);

		$data2['id_subparameter']=$arus;
		$this->db->insert('tb_klasifikasi',$data2);

		$data2['id_subparameter']=$tipologi;
		$this->db->insert('tb_klasifikasi',$data2);

		$data2['id_subparameter']=$vegetasi;
		$this->db->insert('tb_klasifikasi',$data2);

		$data2['id_subparameter']=$garis;
		$this->db->insert('tb_klasifikasi',$data2);

		echo $this->session->set_flashdata('msg','success');
		redirect('backend/klasifikasi');	
	}

	public function excel(){
		$where['tahun']=$this->input->post('tahun');
		$this->db->join('tb_kecamatan','tb_kecamatan.id_kecamatan=tb_bayes.id_kecamatan');
		$this->db->join('tb_desa','tb_desa.id_desa=tb_bayes.id_desa');
		$this->db->join('tb_kabupaten','tb_kabupaten.id_kabupaten=tb_bayes.id_kabupaten');

		$data['klasifikasi']=$this->db->get_where('tb_bayes',$where);
		$this->load->view('backend/v_excel',$data);
	}


}